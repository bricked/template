use std::fs::read_to_string;

fn main() {
    let contents = include_str!("../input.txt");
    let (mut ids1, mut ids2): (Vec<i32>, Vec<i32>) = contents.lines().map(|line| {
        let ids: Vec<i32> = line.split("   ").map(|p| p.parse().unwrap()).collect();
        (ids[0], ids[1])
    }).unzip();

    ids1.sort();
    ids2.sort();

    let id_pairs = ids1.iter().zip(ids2.iter_mut());
    let total_distance = id_pairs.fold(0, |p, (id1, id2)| p + (*id1 - *id2).abs());

    println!("The total distance is {}.", total_distance);
}
